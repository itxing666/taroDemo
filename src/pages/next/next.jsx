import Taro, { Component } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import './next.less'

export default class Next extends Component {

  state = {
    nextTheme: ''
  }

  componentWillMount () {
    console.log(this.$router.params) // 输出 {nextTheme: "大保健"}
    const { nextTheme } = this.$router.params
    this.setState({
      nextTheme
    })
  }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  config = {
    navigationBarTitleText: 'next页面'
  }

  render () {
    const { nextTheme } = this.state
    return (
      <View className='next'>
        <Text>吃好了，来个{nextTheme}全套服务吧！</Text>
      </View>
    )
  }
}
