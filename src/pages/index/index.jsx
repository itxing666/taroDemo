import Taro, { Component } from '@tarojs/taro'
import { View, Text, Button } from '@tarojs/components'
import Child from "../../components/child";
import './index.less'
// componentWillMount () { }

// componentDidMount () { }

// componentWillUnmount () { }

// componentDidShow () { }

// componentDidHide () { }
export default class Index extends Component {

  config = {
    navigationBarTitleText: '首页'
  }

  state = {
    name: "hello 前端骚年"
  }

  goNext() {
    Taro.navigateTo({url:'/pages/next/next?nextTheme=大保健'})
  }

  render () {
    const { name } = this.state
    return (
      <View className='index'>
        <Text>{name}</Text>
        <Child childName="蒜末child"/>
        <Button onClick={this.goNext}>我吃好了</Button>
      </View>
    )
  }
}
